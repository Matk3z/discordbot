import sqlite3
import time
import operator
import image


class DataBase:

    def __init__(self):
        self.conn = sqlite3.connect('content/img.db')
        self.cursor = self.conn.cursor()

        self.cursor.execute("""
CREATE TABLE IF NOT EXISTS imageData(
     id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
     discordID INTERGER,
     discordName TEXT,
     keyWord TEXT,
     filePath TEXT,
     date TEXT,
     serverID INTEGER,
     useNumber INTEGER,
     lastUsed TEXT,
     nsfw INTEGER

)
""")

        self.conn.commit()

    def SetData(self, discordID, discordName, keyword, filePath, serverID, useNumber, nsfw):
        discordName = str(discordName)
        date = time.ctime()
        self.cursor.execute(
        """INSERT INTO imageData(discordID, discordName, keyWord, filePath, date, serverID, useNumber, nsfw) VALUES(?, ?, ?, ?, ?, ?, ?, ?)""",
        (discordID, discordName, keyword, filePath, date, serverID, useNumber, nsfw)
        )
        self.conn.commit()

    def DelData(self, keyword):

        self.cursor.execute("""SELECT filepath FROM imageData WHERE keyword=?""", (keyword,))
        fp = self.cursor.fetchone()[0]

        image.Image().DelImg(fp)

        self.cursor.execute("""DELETE FROM imageData WHERE keyWord=?""", (keyword,))
        self.conn.commit()

    def UsedImage(self, keyword):

        self.cursor.execute("""SELECT useNumber FROM imageData WHERE keyWord=?""", (keyword,))
        response = self.cursor.fetchone()

        date = time.ctime()

        response = response[0] + 1
        print(response)

        self.cursor.execute("""UPDATE imageData SET useNumber=? WHERE keyword=? """, (response,keyword,))
        self.cursor.execute("""UPDATE imageData SET lastUsed=? WHERE keyword=? """, (date,keyword,))
        self.conn.commit()

    def SearchImage(self, keyword):

        self.cursor.execute("""SELECT keyWord FROM imageData WHERE keyWord LIKE ?""", (keyword+'%',))
        response1 = self.cursor.fetchall()
        self.cursor.execute("""SELECT keyWord FROM imageData WHERE keyWord LIKE ? AND keyWord NOT LIKE ?""", ('%'+keyword+'%',keyword+'%',))
        response2 = self.cursor.fetchall()
        wordValue = []

        for i in range (len(response2)):

            wordValue.append([response2[i][0], (len(keyword) / len(response2[i][0])) * 100])

        wordValue = sorted(wordValue, key=operator.itemgetter(1), reverse=True)

        for i in range(len(wordValue)):
            response2[i] = wordValue[i]

        response = response1 + response2
        return response

    def RetrieveData(self, keyword):

        """return a dict {discordID, discordName, keyword, fp, date, serverID, useNumber, lastUsed, nsfw}"""

        self.cursor.execute("""SELECT * FROM imageData WHERE keyword=?""", (keyword,))

        response = self.cursor.fetchone()
        try:
            dataDict = {'discordID' : response[1], 'discordName' : response[2], 'keyword' : response[3], 'fp' : response[4], 'date' : response[5], 'serverID' : response[6], 'useNumber' : response[7], 'lastUsed' : response[8], 'nsfw' : response[9]}
        except TypeError:
            dataDict = None
        return dataDict

    def GetAllKeyword(self):

        """Return a List of all the keyword in the database"""

        self.cursor.execute("""SELECT keyWord FROM imageData""")
        response = self.cursor.fetchall()

        return response

    def SetNsfw(self, keyword, nsfw):

        self.cursor.execute("""UPDATE imageData SET nsfw=? WHERE keyWord=?""", (nsfw, keyword))
        self.conn.commit()
