import discord
import time
import random
import image
import database
import math

"""
Main File for Cool king a Discord bot using discord API to stock images and retrieve them
"""


def CreateHelpMsg():

    helpMsg1 = """`.stockimg <keyword>,<nsfw>` Stock l'image sur un serveur distant le mot-clé (keyword) permettra de la récupérer. Attention un  mot clé peut etre utilisé une seul fois et si plusieurs image sont ajouté au message seul la première sera prise en compte. Par defaut si nsfw n'est pas ajouté nsfw = False
`Exemple : .stockimg foo, nsfw`"""
    helpMsg2 = """`.getimg <keyword>` Récupere l'image precedement stocké avec le mot clé
`Exemple : .getimg foo`"""
    helpMsg3 = """`.delimg <keyword>` Supprime une l'image enregistrer en fonction du mot clé (fonctionne seulement si c'est le propriétaire de l'image qui effectue la commande)
`Exemple : .delimg foo`"""
    helpMsg4 = """`.search <chaine_de_caractere>` permet de chercher un image dans la base de donnée en fonction de la chaine de caractère donnée (donne en priorité les mots clés qui commence par cette chaine
`Exemple : .search foo or .search foo, 2`"""
    helpMsg5 = """`.info <keyword>` Donne des informations sur le mot clé demandé """
    helpMsg6 = """```\n\n# Don't include exemple brackets <> into the command``` """

    helpMsgLst = [helpMsg1, helpMsg2, helpMsg3, helpMsg4, helpMsg5, helpMsg6]
    helpMsg = ''

    for i in range(len(helpMsgLst)):
        helpMsg = helpMsg + helpMsgLst[i] + '\n' + '\n'

    return helpMsg


client = discord.Client()
image = image.Image()
dataBase = database.DataBase()

wordDict = {}



@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))

@client.event
async def on_guild_join(guild):
    for i in range(len(guild.channels)):
        if str(type(guild.channels[i]))[24:35] == 'TextChannel' and guild.channels[i].position == 0:

            await guild.channels[i].send('```Hello World```')
            helpMsg = CreateHelpMsg()

            await guild.channels[i].send(helpMsg)
            break


@client.event
async def on_reaction_add(reaction, user):

    reactKeyword = reaction.message.content.replace('>', '<').split("<")[1]
    page = reaction.message.content.replace('>', '<').split("<")[2][8]


    if reaction.message.content[0:10] == '`searching':
        if str(reaction) == '⬅' and reaction.count == 2:
            await reaction.message.channel.send('.search {}, {}'.format(reactKeyword, int(page) - 1))
            await reaction.message.delete()
        elif str(reaction) == '➡' and reaction.count == 2:
            await reaction.message.channel.send('.search {}, {}'.format(reactKeyword, int(page) + 1))
            await reaction.message.delete()




@client.event
async def on_message(message):

    date = time.ctime()

    message.content = message.content.lower()

    if message.content[0:7] == '.search' and message.author == client.user:
        await message.delete()

    if message.content[0:10] == '`searching':

        page = message.content.replace('>', '<').split("<")[2][8]
        maxPage = message.content.replace('>', '<').split("<")[2][13]


        if int(page) == 1 and int(page) != int(maxPage):
            await message.add_reaction('➡')
            return
        elif int(page) == 1 and int(page) == int(maxPage):
            return
        elif int(page) == int(maxPage):
            await message.add_reaction('⬅')
            return
        else:
            await message.add_reaction('⬅')
            await message.add_reaction('➡')
            return
        


    if message.author == client.user and message.content[0:7] != '.search':
        return

    #Don't write code above this comment



    if message.content[:6] == '`.hello`':
        msg = 'Hello{0.author.mention}'.format(message)
        await message.channel.send(msg)


    if len(message.attachments) > 0:
        image.SetImgChannel(message.channel)
        image.SetImgUrl(message.attachments)
        image.SetImgType(image.imgUrl)


    if message.content[:9] == '.stockimg':

        if image.imgType == None:
            await message.channel.send('`File type is not correct`')
            return
            
        if image.channel != message.channel:
            await message.channel.send('`Attach image directly to the message`')
            return

        content = message.content[10:]
        content = content.strip()

        if len(content) == 0:
            await message.channel.send('`KeyWord is incorrect. Type .Stockimg keyword`')
            return

        content = content.split(',')
        keyword = content[0]

        if dataBase.RetrieveData(keyword) != None:
            await message.channel.send('`Keyword already exist`')
            return
        
        if len(content) == 2 and content[1].strip() == 'nsfw':
            nsfw = 1
        elif len(content) == 2 and content[1].strip() != 'nsfw':
            await message.channel.send('`Command is incorrect. Try : .Stockimg keyword, nsfw`')
            return
        else:
            nsfw = 0

        fp = image.SaveImg(keyword, nsfw)
        allKeyword = dataBase.GetAllKeyword()
        
        for i in range(len(allKeyword)):
            keywordData = dataBase.RetrieveData(allKeyword[i][0])

            if open(keywordData['fp'], 'rb').read() == open(fp, 'rb').read():
                await message.channel.send('`Image already in this bot. To avoid duplicates images can only be saved once.` \n`Image is stored with keyword : {}` '.format(keywordData['keyword']))
                return

        dataBase.SetData(message.author.id,message.author, keyword, fp, message.guild.id, 1, nsfw)
        await message.channel.send('`Image successfully stored`')
        print('({}) {} has been stocked under {}'.format(date, keyword, fp))

    if message.content[:7] == '.getimg':

        keyword = message.content[8:]

        response = dataBase.RetrieveData(keyword)
        if response == None:

            await message.channel.send("`Keyword doesn't exist`")
            return

        imageFile = discord.File(response['fp'])
        dataBase.UsedImage(keyword)

        await message.channel.send('`Image from keyword : {}` '.format(keyword), file = imageFile)
    
    if message.content[:7] == '.search':
        
        content = message.content[8:]
        content = content.split(',')
        
        keyword = content[0].strip()

        if len(keyword) == 0:
            await message.channel.send('`Command is incorrect. Try : .search keyword, 1(or 2,3,4,...)`')
            return

        responseList = dataBase.SearchImage(keyword)

        if len(responseList) == 0:
            await message.channel.send("`No KeyWord found`")
            return
        page = 1
        if len(content) == 2:
            try:
                page = int(content[1])
            except ValueError:
                await message.channel.send("`Command is incorrect. Try : .search keyword, 1(or 2,3,4,...)`")
                return
        
        maxPage = math.ceil(len(responseList) / 10)

        if page < 1:
            await message.channel.send("`page can't be negative or = 0`")
            return

        try:
            responseList[(page - 1) * 10][0]
        except IndexError:
            await message.channel.send('`Max page is {}` '.format(maxPage))
            return
            


        responseStr = ''        

        for i in range( 10 * (page - 1), len(responseList)):

            if i == 10 + 10 * (page - 1):
                break

            responseStr = responseStr + '{}'.format((i + 1)) + '. ' + responseList[i][0] + '\n'
        
        if page < maxPage:
            pageInfo = '`To show next page write .search {}, {}`'.format(keyword, page + 1)
        else:
            pageInfo = ''

        await message.channel.send("""`Searching < {} >`\n`Page {} of {}` """.format(keyword, page, maxPage) + '```' + responseStr + '```' + pageInfo)

    if message.content[:5] == '.info':

        keyword = message.content[6:]
        keyWordData = dataBase.RetrieveData(keyword)
        if keyWordData['nsfw'] == 1:
            nsfw = True
        else:
            nsfw = False

        msg = '```{} info : \n \nImage stocked by {} on {} \nThis image has been called {} time(s) and the last one was on {}\nNsfw = {}```'.format(keyword, keyWordData['discordID'], keyWordData['discordName'], keyWordData['useNumber'], keyWordData['lastUsed'], nsfw)


        await message.channel.send(msg)

    if message.content[:7] == '.random' and message.content[:11] != '.randomnsfw':
        nsfw = 1
        while nsfw == 1:

            response = dataBase.GetAllKeyword()
            randomInt = random.randint(0, len(response) - 1)
            keyWordData = dataBase.RetrieveData(response[randomInt][0])
            nsfw = keyWordData['nsfw']


        imgData = dataBase.RetrieveData(response[randomInt][0])['fp']

        imageFile = discord.File(imgData)

        await message.channel.send('`Image from keyword : {}` '.format(response[randomInt][0]), file = imageFile)
        await message.channel.send('`if you get a nsfw image type .nsfw keyword in the chat` ')

    if message.content[:11] == '.randomnsfw':

        nsfw = 0
        while nsfw == 0:
            response = dataBase.GetAllKeyword()
            randomInt = random.randint(0, len(response) - 1)
            keyWordData = dataBase.RetrieveData(response[randomInt][0])
            nsfw = keyWordData['nsfw']

        imgData = dataBase.RetrieveData(response[randomInt]['fp'])

        imageFile = discord.File(imgData[0])
        await message.channel.send('`Image from keyword : {}` '.format(response[randomInt][0]), file = imageFile)


    if message.content[:5] == '.nsfw':

        keyword = message.content[6:]
        dataBase.SetNsfw(keyword, 1)
        keyWordData = dataBase.RetrieveData(keyword)
        if keyWordData == None:
            await message.channel.send("`KeyWord doesn't exist`")
        if keyWordData['nsfw'] == 0:
            await message.channel.send('`A bug has occured try again`')
            return
        else:
            await message.channel.send('`Image successfully set nsfw`')

        print('({}) {} set nsfw'.format(date, keyword))

    if message.content[:8] == '.nonnsfw':

        keyword = message.content[9:]
        dataBase.SetNsfw(keyword, 0)
        keyWordData = dataBase.RetrieveData(keyword)
        if keyWordData == None:
            await message.channel.send("`KeyWord doesn't exist`")
        if keyWordData['nsfw'] == 1:
            await message.channel.send('`A bug has occured try again`')
            return
        else:
            await message.channel.send('`Image successfully set non-nsfw`')

        print('({}) {} set non-nsfw'.format(date, keyword))

    if message.content[:7] == '.delimg':

        keyword = message.content[8:]
        keyWordData = dataBase.RetrieveData(keyword)

        if type(keyword) == None:
            await message.channel.send("Keyword is invalid or doesnt't exist")
            return

        if message.author.id != keyWordData['discordID'] and message.author.id != 145588451486597120:
            await message.channel.send('`You are not the owner of this keyword`')
            return

        dataBase.DelData(keyword)
        response = dataBase.SearchImage(keyword)

        if len(response) == 0:
            await message.channel.send("`keyword deleted`")
            return

        for i in range(len(response)):

            if response[i] == keyword:
                await message.channel.send("`A bug as occured try again`")
                return

        await message.channel.send("`keyword deleted`")
        print('({}) {} has been deleted'.format(date, keyword))                

    if message.content[:5] == '.help':
        
        helpMsg = CreateHelpMsg()

        await message.channel.send(helpMsg)

client.run('NTExODA4ODI5NzY0MjcyMTI4.DswTFg.OjnfVrfXCTy02Kti2caz_lJHGLo')