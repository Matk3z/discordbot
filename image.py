import requests
import uuid
import os



class Image:


    def SetImgUrl(self, attachments):
        
        self.msgAttachments = attachments

        self.imgUrl = attachments[0].url

    def SetImgType(self, imgUrl):

        if imgUrl[(len(imgUrl) - 4):] == '.png':
            self.imgType = 'png'
        elif imgUrl[(len(imgUrl) - 4):] == '.jpg':
            self.imgType = 'jpg'
        elif imgUrl[(len(imgUrl) - 5):] == '.jpeg':
            self.imgType = 'jpeg'
        elif imgUrl[(len(imgUrl) - 4):] == '.gif':
            self.imgType = 'gif'
        else:
            self.imgType = None


    def SaveImg(self, keyWord, nsfw):

        """Return the Filepath and save the image on the disk"""
        if self.imgType != None:
            img = requests.get(self.imgUrl).content
            fileName = '{0}.{1}'.format(uuid.uuid4(), self.imgType)
            fp = "content/image/{0}".format(fileName)
            with open(fp, 'wb') as outfile:
                outfile.write(img)

        return fp
        print('FileSaved')


    def DelImg(self, fp):

        os.remove(fp)
    
    def SetImgChannel(self, channel):
        self.channel = channel
