# Working on virtual env

## Requirements for Windows

Install venv:

    $ pip install virtualenvwrapper-win

Create virtual venv (only once):

    $ mkvirtualenv discordbot

Start using venv:

    $ workon discordbot

Stop using venv:

    $ deactivate

## Installation

    $ pip install -r requirements.txt

## Usage



    $ python discordbot.py

## Bot invite

    https://discordapp.com/oauth2/authorize?client_id=511808829764272128&scope=bot&permissions=8